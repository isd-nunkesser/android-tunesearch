package de.hshl.isd.tunesearch

data class SearchRequest(val term: String)