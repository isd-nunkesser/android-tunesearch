package de.hshl.isd.tunesearchcompose.core.ports

data class SearchTracksDTO(val term: String)