package de.hshl.isd.tunesearchcompose.core.ports

data class CollectionEntity(val name: String, val tracks: List<TrackEntity>)