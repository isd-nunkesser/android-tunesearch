package de.hshl.isd.tunesearchcompose

data class SearchRequest(val term: String)