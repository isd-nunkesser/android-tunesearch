package de.hshl.isd.tunesearchcompose

data class CollectionViewModel(val name: String, val tracks: List<TrackViewModel>)