package de.hshl.isd.tunesearchcompose

data class ResponseEntity(val resultCount : Int, val results: List<TrackEntity>)

